'use strict'

const {
  after,
  assert,
  assertx,
  beforeEach,
  describe,
  it,
} = require('./harness')
const fsp = require('node:fs/promises')
const cheerio = require('cheerio')
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const WORK_DIR = ospath.join(__dirname, 'work')

const generateSite = require('@antora/site-generator')

describe('generateSite()', () => {
  const cacheDir = ospath.join(WORK_DIR, '.cache/antora')
  const outputDir = ospath.join(WORK_DIR, 'public')
  const defaultPlaybookFile = ospath.join(
    FIXTURES_DIR,
    'docs-site/antora-playbook.yml'
  )
  const playbookFileWithCustomSupplementalUi = ospath.join(
    FIXTURES_DIR,
    'docs-site',
    'antora-playbook-with-custom-supplemental-ui.yml'
  )

  beforeEach(() => fsp.rm(outputDir, { recursive: true, force: true }))
  after(() => fsp.rm(WORK_DIR, { recursive: true, force: true }))

  it('should generate a site with a search index', async () => {
    const env = {}
    await generateSite(
      [
        '--playbook',
        defaultPlaybookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_PROVIDER')
    const searchIndexPath = ospath.join(outputDir, 'search-index.js')
    assertx.file(searchIndexPath)
    global.lunr = {}
    global.antoraSearch = {}
    global.antoraSearch.initSearch = function (lunr, index) {
      assert.equal(Object.keys(index.store.documents).length, 2)
      const documents = Object.entries(index.store.documents).map(
        ([key, value]) => ({
          title: value.title,
          url: value.url,
        })
      )
      assert.equal(documents.length, 2)
      assertx.hasProperties(documents[0], {
        title: 'Antora x Lunr',
        url: '/antora-lunr/index.html',
      })
      assertx.hasProperties(documents[1], {
        title: 'Page Title',
        url: '/antora-lunr/named-module/the-page.html',
      })
    }
    require(searchIndexPath)
    delete global.lunr
    delete global.antoraSearch
  })

  it('should insert script element with predefined data attributes', async () => {
    await generateSite(
      [
        '--playbook',
        defaultPlaybookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const startPageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/index.html')
    )
    let $ = cheerio.load(startPageContents)
    assertx.one($('#search-input'))
    let searchScript = $('#search-ui-script')
    assertx.one(searchScript)
    assert.equal(searchScript.attr('data-site-root-path'), '..')
    assert.equal(searchScript.attr('data-stylesheet'), '../_/css/search.css')
    assert.equal(searchScript.attr('data-snippet-length'), '100')
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/named-module/the-page.html')
    )
    $ = cheerio.load(thePageContents)
    searchScript = $('#search-ui-script')
    assert.equal(searchScript.attr('data-site-root-path'), '../..')
    assert.equal(searchScript.attr('data-stylesheet'), '../../_/css/search.css')
  })

  it('should output lunr.js client/engine to js vendor directory of UI output folder', async () => {
    await generateSite(
      [
        '--playbook',
        defaultPlaybookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const expected = ospath.join(outputDir, '_/js/vendor/lunr.js')
    assertx.sameFile(expected, require.resolve('lunr/lunr.min.js'))
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/named-module/the-page.html')
    )
    const $ = cheerio.load(thePageContents)
    assertx.one($('script[src="../../_/js/vendor/lunr.js"]'))
  })

  it('should output vendored JS files to multiple destinations', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-destinations.yml'
    )
    await generateSite(
      ['--playbook', playbookFile, '--cache-dir', cacheDir, '--quiet'],
      {}
    )
    const expectedA = ospath.join(outputDir, 'a', '_/js/vendor/lunr.js')
    assertx.sameFile(expectedA, require.resolve('lunr/lunr.min.js'))
    const expectedB = ospath.join(outputDir, 'b', '_/js/vendor/lunr.js')
    assertx.sameFile(expectedB, require.resolve('lunr/lunr.min.js'))
  })

  it('should output language support files to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-languages.yml'
    )
    const env = {}
    await generateSite(
      [
        '--playbook',
        playbookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.fr.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.de.js')),
    ]).then(Buffer.concat)
    assertx.contents(
      ospath.join(outputDir, '_/js/vendor/lunr-languages.js'),
      expectedContents.toString()
    )
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/named-module/the-page.html')
    )
    const $ = cheerio.load(thePageContents)
    assertx.one($('script[src="../../_/js/vendor/lunr-languages.js"]').get())
    assertx.empty(
      $('script[src="../../_/js/vendor/lunr.stemmer.support.js"]').get()
    )
    assertx.empty($('script[src="../../_/js/vendor/lunr.fr.js"]').get())
  })

  it('should output language support files[ja] to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-languages-ja.yml'
    )
    const env = {}
    await generateSite(
      [
        '--playbook',
        playbookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/tinyseg.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.ja.js')),
    ]).then(Buffer.concat)
    assertx.contents(
      ospath.join(outputDir, '_/js/vendor/lunr-languages.js'),
      expectedContents.toString()
    )
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/named-module/the-page.html')
    )
    const $ = cheerio.load(thePageContents)
    assertx.empty(
      $('script[src="../../_/js/vendor/lunr.stemmer.support.js"]').get()
    )
    assertx.one($('script[src="../../_/js/vendor/lunr-languages.js"]').get())
    assertx.empty($('script[src="../../_/js/vendor/tinyseg.js"]').get())
    assertx.empty($('script[src="../../_/js/vendor/lunr.ja.js"]').get())
  })

  it('should output language support files[th] to js vendor directory of UI output folder', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-languages-th.yml'
    )
    const env = {}
    await generateSite(
      [
        '--playbook',
        playbookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_LANGUAGES')
    const expectedContents = await Promise.all([
      fsp.readFile(require.resolve('lunr-languages/lunr.stemmer.support.js')),
      fsp.readFile(require.resolve('lunr-languages/wordcut.js')),
      fsp.readFile(require.resolve('lunr-languages/lunr.th.js')),
    ]).then(Buffer.concat)
    assertx.contents(
      ospath.join(outputDir, '_/js/vendor/lunr-languages.js'),
      expectedContents.toString()
    )
    const thePageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/named-module/the-page.html')
    )
    const $ = cheerio.load(thePageContents)
    assertx.one($('script[src="../../_/js/vendor/lunr-languages.js"]').get())
    assertx.empty(
      $('script[src="../../_/js/vendor/lunr.stemmer.support.js"]').get()
    )
    assertx.empty($('script[src="../../_/js/vendor/wordcut.js"]').get())
    assertx.empty($('script[src="../../_/js/vendor/lunr.th.js"]').get())
  })

  it('should allow extension to configure snippet length', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-snippet-length.yml'
    )
    await generateSite(
      [
        '--playbook',
        playbookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const startPageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/index.html')
    )
    const searchScript = cheerio.load(startPageContents)('#search-ui-script')
    assert.equal(searchScript.attr('data-snippet-length'), '250')
  })

  it('should use existing search-scripts.hbs partial if present in UI', async () => {
    await generateSite(
      [
        '--playbook',
        playbookFileWithCustomSupplementalUi,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const startPageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/index.html')
    )
    const searchScript = cheerio.load(startPageContents)('#search-ui-script')
    assertx.one(searchScript)
    assert.equal(searchScript.attr('data-snippet-length'), '150')
    assert.equal(searchScript.attr('data-stylesheet'), undefined)
  })

  it('should not generate search index or add scripts to pages if extension is not enabled', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-without-extension.yml'
    )
    const env = {}
    await generateSite(
      [
        '--playbook',
        playbookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      env
    )
    assertx.doesNotHaveProperty(env, 'SITE_SEARCH_PROVIDER')
    assertx.notPath(ospath.join(outputDir, 'search-index.js'))
    const startPageContents = await fsp.readFile(
      ospath.join(outputDir, 'antora-lunr/index.html')
    )
    const $ = cheerio.load(startPageContents)
    assertx.empty($('#search-input'))
    assertx.empty($('#search-ui-script'))
  })

  it('should throw error if unknown options are specified in playbook', async () => {
    const playbookFile = ospath.join(
      FIXTURES_DIR,
      'docs-site',
      'antora-playbook-with-unknown-options.yml'
    )
    const expected = new Error(
      'Unrecognized options specified for @antora/lunr-extension: foo, yin'
    )
    await assert.rejects(
      generateSite(['--playbook', playbookFile], {}),
      expected
    )
  })

  it('should output search.css to css directory of UI output folder', async () => {
    await generateSite(
      [
        '--playbook',
        defaultPlaybookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const actual = ospath.join(outputDir, '_/css/search.css')
    const expected = ospath.join(__dirname, '..', 'data', 'css', 'search.css')
    assertx.sameFile(actual, expected)
  })

  it('should output search-ui.js to js directory of UI output folder', async () => {
    await generateSite(
      [
        '--playbook',
        defaultPlaybookFile,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const actual = ospath.join(outputDir, '_/js/search-ui.js')
    const expected = ospath.join(__dirname, '..', 'data', 'js', 'search-ui.js')
    assertx.sameFile(actual, expected)
  })

  it('should preserve existing search.css', async () => {
    await generateSite(
      [
        '--playbook',
        playbookFileWithCustomSupplementalUi,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const actual = ospath.join(outputDir, '_/css/search.css')
    const expected = ospath.join(
      __dirname,
      'fixtures',
      'docs-site',
      'custom-supplemental-ui',
      'css',
      'search.css'
    )
    assertx.sameFile(actual, expected)
  })

  it('should preserve existing search-ui.js', async () => {
    await generateSite(
      [
        '--playbook',
        playbookFileWithCustomSupplementalUi,
        '--to-dir',
        outputDir,
        '--cache-dir',
        cacheDir,
        '--quiet',
      ],
      {}
    )
    const actual = ospath.join(outputDir, '_/js/search-ui.js')
    const expected = ospath.join(
      __dirname,
      'fixtures',
      'docs-site',
      'custom-supplemental-ui',
      'js',
      'search-ui.js'
    )
    assertx.sameFile(actual, expected)
  })
})
