'use strict'

process.env.NODE_ENV = 'test'

const assert = require('node:assert/strict')
const assertx = require('./assertx')
const { after, beforeEach, describe, it } = require('node:test')
const { configureLogger } = require(require.resolve('@antora/logger', {
  paths: [require.resolve('@antora/site-generator')],
}))
const ContentCatalog = require(require.resolve(
  '@antora/content-classifier/lib/content-catalog',
  {
    paths: [require.resolve('@antora/site-generator')],
  }
))

const buildContentCatalog = (playbook, files = []) => {
  const catalog = new ContentCatalog(playbook)
  const contentVersionKeys = new Set()
  files.forEach((file) => {
    file = Object.assign({ asciidoc: {} }, file)
    const src = (file.src = Object.assign(
      { module: 'ROOT', family: 'page', relative: 'index.adoc' },
      file.src
    ))
    const { component, version } = src
    const contentVersionKey = `${version}@${component}`
    if (!contentVersionKeys.has(contentVersionKey)) {
      catalog.registerComponentVersion(component, version)
      contentVersionKeys.add(contentVersionKey)
    }
    catalog.addFile(file)
  })
  return catalog
}

module.exports = {
  after,
  assert,
  assertx,
  beforeEach,
  describe,
  buildContentCatalog,
  configureLogger,
  it,
}
